declare module Store {
    interface BaseAction<T = any> {
        type: string;
        payload?: T;
    }

    interface State {
        articles: {
            articles: Array<App.Article>,
            isLoading: false,
            isError: false,
        }
    }
}

declare module App {
    interface ButtonDataItem {
        id: number;
        name: string;
        fetchParam: string;
    }

    interface Article {
        title: string;
        url: string;
        id: string;
        isLiked: boolean;
    }

    interface CachedArticle {
        data: Article,
        expired: number
    }
}

declare module Sagas {
    type fetchFunction =(() => Promise<App.Article> ) | (() => App.Article);
    
    interface FetchArticleAction {
        fetchFunction: fetchFunction,
        type: string
    }
}

