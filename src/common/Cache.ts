const ArticleCache = new Map<string, App.CachedArticle>(JSON.parse(localStorage.ArticleCache || null))
export default ArticleCache;