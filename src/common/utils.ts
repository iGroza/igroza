import React from 'react';

function getRandomElementFromArray<T extends any>(array: Array<T>) {
    return array[Math.floor(Math.random() * array.length)];
}

function ellipsisText(text: string = "", maxlimit: number): string {
    return ((text).length > maxlimit) ? (((text).substring(0, maxlimit - 3)) + '...') : text
}

function generateExpiredTimestamp(expiredAfterMin: number) {
    return Date.now() + expiredAfterMin * 1000 * 60;
}

export default {
    getRandomElementFromArray,
    generateExpiredTimestamp,
    ellipsisText,
}