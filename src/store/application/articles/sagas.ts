import { CONSTANTS } from './constants';
import { takeEvery, put, call } from "redux-saga/effects"
import ARTICLE_ACTIONS from './action';

function* watchFetchArticles() {
    yield takeEvery(CONSTANTS.FETCHED_ARTICLE, fetchArticleAsync)
}

function* fetchArticleAsync({ fetchFunction }: Sagas.FetchArticleAction) {
    try {
        yield put(ARTICLE_ACTIONS.requestArticle())
        const article = yield call(() => fetchFunction())
        yield put(ARTICLE_ACTIONS.requestArticleSuccess(article))
    } catch (e) {
        yield put(ARTICLE_ACTIONS.requestArticleFailed(e))
    }
}

export default {
    watchFetchArticles
}