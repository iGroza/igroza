import { CONSTANTS } from './constants';

const initialState = {
    articles: new Array<App.Article>(),
    isLoading: false,
    isError: false,
};

export const articlesReducer = (state = initialState, action: Store.BaseAction) => {
    switch (action.type) {
        case CONSTANTS.REQUEST_ARTICLE: {
            return { ...state, isLoading: true };
        }
        case CONSTANTS.REQUEST_ARTICLE_SUCCESS: {
            let { articles } = state;
            articles = [...articles, action.payload];
            return { ...state, isLoading: false, articles };
        }
        case CONSTANTS.REQUEST_ARTICLE_FAILED: {
            return { ...state, isLoading: false, isError: true };
        }
        case CONSTANTS.DELETE_ARTICLE: {
            const { articleId } = action.payload
            let { articles } = state;
            articles = articles.filter(article => article.id !== articleId)
            return { ...state, articles };
        }
        case CONSTANTS.LIKE_ARTICLE: {
            const { articleId } = action.payload
            let { articles } = state;
            articles = articles.map(
                article => articleId === article.id ?
                    {
                        ...article,
                        isLiked: !article.isLiked,
                    }
                    : article)
            return { ...state, articles };
        }
        default: return state;
    }
};
