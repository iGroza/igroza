import { CONSTANTS } from './constants'

function fetchedArticle(fetchFunction: Sagas.fetchFunction){
    return {
        type: CONSTANTS.FETCHED_ARTICLE,
        fetchFunction
    }
}

function requestArticle() {
    return {
        type: CONSTANTS.REQUEST_ARTICLE
    }
}

function requestArticleSuccess(article: App.Article){
    return {
        type: CONSTANTS.REQUEST_ARTICLE_SUCCESS,
        payload: {
            ...article,
        }
    }
}

function requestArticleFailed(error: Error){
    return {
        type: CONSTANTS.REQUEST_ARTICLE_FAILED,
        payload: {
            error,
        }
    }
}

function likeArticle(articleId: string){
    return {
        type: CONSTANTS.LIKE_ARTICLE,
        payload: {
            articleId
        }
    }
}

function deleteArticle(articleId: string){
    return {
        type: CONSTANTS.DELETE_ARTICLE,
        payload: {
            articleId
        }
    }
}

const ARTICLE_ACTIONS = {
    fetchedArticle,
    requestArticle,
    requestArticleSuccess,
    requestArticleFailed,
    likeArticle,
    deleteArticle
}

export default ARTICLE_ACTIONS