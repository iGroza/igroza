import { combineReducers } from 'redux'
import { articlesReducer } from "./articles/reducer"

const state = {
   articles: articlesReducer
}

export default combineReducers(state)
