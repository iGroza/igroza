import { applyMiddleware, compose, createStore } from 'redux'
import rootReducer from './application/rootReducer';
import createSagaMiddleware from "redux-saga";
import ArticleSagas from "./application/articles/sagas";

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers =
    typeof window === 'object' &&
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(sagaMiddleware),
);

const store = createStore(rootReducer, enhancer)

sagaMiddleware.run(ArticleSagas.watchFetchArticles)

export default store
