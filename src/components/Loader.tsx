import React from "react";
import styled from "styled-components";

interface IProps {
    visibility: number;
}
const Loader = styled.div<IProps>`
    background: url(https://igroza.gitlab.io/igroza/images/loader.svg) center center no-repeat;
    background-size: contain;
    width: 10rem;
    height: 5rem;
    opacity: ${props => props.visibility}
`;

export default Loader;