import { link } from "fs/promises"
import React from "react"
import styled from "styled-components"

const Link = styled.a`
    color: inherit; 
    text-decoration: inherit; 
`

export default Link