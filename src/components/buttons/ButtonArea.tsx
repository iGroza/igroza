import React from 'react';
import styled from 'styled-components';

const ButtonArea = styled.div`
 width: 100%;
 display: flex;
 flex-direction: column;
 justify-content: space-between;
 min-height: 12.5rem;
`;

export default ButtonArea;