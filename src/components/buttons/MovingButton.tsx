import React from 'react';
import styled from 'styled-components';
import Button from './Button';

interface IProps {
  movement?: string;
  movementTime?: string;
  opacity: number;
};

const MovingButton = styled(Button) <IProps>`
  animation: horizontalMovement ${params => params.movementTime ? params.movementTime : "5s"} ease-in-out infinite;
  opacity: ${props => props.opacity};

  :hover {
    background-color: #0277BD;
    animation-play-state: paused;
  }

  @keyframes horizontalMovement {
    50% {
      transform: translateX(${params => params.movement ? params.movement : "100%"})
    }
  }
`;

export default MovingButton;