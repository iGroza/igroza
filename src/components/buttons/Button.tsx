import React from 'react';
import styled from 'styled-components';

const Button = styled.div`
  width: 10rem;
  text-align: center;
  user-select: none;
  padding: 0.5rem 1rem;
  font-family: Gilroy;
  font-size: 2rem;
  background-color: #01579B;
  color: #FFFFFF;
  border-radius: 2px;
  
  :active {
    background-color: #0277BD;
  }
`;

export default Button;