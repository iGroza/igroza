import React from "react"
import { useDispatch } from 'react-redux'
import ARTICLE_ACTIONS from "../../store/application/articles/action"
import ButtonArea from "../buttons/ButtonArea"
import MovingButton from "../buttons/MovingButton"
import utils from "../../common/utils";
import { v4 } from 'uuid'
import ArticleCache from "../../common/Cache"

const buttosData: Array<App.ButtonDataItem> = [
    {
        id: 0,
        name: "Frontend",
        fetchParam: "frontend"
    },
    {
        id: 1,
        name: "ReactJS",
        fetchParam: "reactjs"
    },
    {
        id: 2,
        name: "VueJS",
        fetchParam: "vuejs"
    },
    {
        id: 3,
        name: "Angular",
        fetchParam: "angular"
    },
];

const redditArticleDataExtarctor = (article): App.Article => {
    return {
        id: v4(),
        title: article.data.title,
        url: article.data.url,
        isLiked: false
    }
}

const fetchFunctionFabric = (article) => {
    const url = `https://www.reddit.com/r/${article}/.json`

    if (ArticleCache.has(url) && ArticleCache.get(url).expired > Date.now()) {
        return () => ({
            ...ArticleCache.get(url).data,
            id: v4()
        })
    }

    return () => fetch(url, {
        cache: "force-cache"
    })
        .then(data => data.json())
        .then(json => json.data.children)
        .then(articlesArray => utils.getRandomElementFromArray(articlesArray))
        .then(article => redditArticleDataExtarctor(article))
        .then(article => {
            ArticleCache.set(url, {
                data: article,
                expired: utils.generateExpiredTimestamp(2)
            })

            localStorage.ArticleCache = JSON.stringify(Array.from(ArticleCache.entries()));

            return article
        })
}

interface IProps {
    isLoading: boolean;
}

const ArticlesButton = ({ isLoading }: IProps) => {
    const dispatch = useDispatch()
    return (
        <ButtonArea>
            {
                buttosData.map(button => (
                    <MovingButton
                        movement="calc(100vw - 100%)"
                        movementTime="10s"
                        opacity={isLoading ? 0.7 : 1}
                        onClick={() => {
                            if (isLoading) return console.warn("[MovingButton] an article still loading...");
                            dispatch(ARTICLE_ACTIONS.fetchedArticle(fetchFunctionFabric(button.fetchParam)));
                        }}
                        key={button.id}
                    >
                        {button.name}
                    </MovingButton>
                ))
            }
        </ButtonArea>);
}

export default ArticlesButton