import React from "react";
import styled from "styled-components";

const ArticlesContainer = styled.div`
    display: flex;
    flex-direction: column;
`;

export default ArticlesContainer;