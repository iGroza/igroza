import React from "react"
import styled from "styled-components"
import Link from "../Link"
import ArticleItem from "./ArticleItem"
import ArticlesContainer from "./ArticlesContainer"

interface IProps {
    articles: Array<App.Article>
}

const ArticlesList = ({ articles }: IProps) => {

    return (
        <ArticlesContainer>
            {
                articles.map(({ id, isLiked, title, url }) => (
                    <ArticleItem key={id} id={id} isLiked={isLiked}>
                        <Link href={url} target="_blank">{title}</Link>
                    </ArticleItem>
                ))
            }
        </ArticlesContainer>
    )
}

export default ArticlesList
