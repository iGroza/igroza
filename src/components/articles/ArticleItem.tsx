import React from 'react';
import styled from 'styled-components';
import { useDispatch } from "react-redux";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as farHeart } from "@fortawesome/free-regular-svg-icons";
import { faTrash as fasTrash, faHeart as fasHeart } from "@fortawesome/free-solid-svg-icons";
import Loader from '../Loader';
import ARTICLE_ACTIONS from '../../store/application/articles/action';

library.add(fasTrash, fasHeart, farHeart)

const ItemContainer = styled.div`
    display: flex;
    align-items: end;
    font-size: 1.5rem;

    & > * {
        margin-left: 0.5rem;
    }
`

const Item = styled.div`
    font-family: Gilroy;
    font-size: 1.5rem;
    color: #CFD8DC;
    text-decoration: underline;
    cursor: pointer;
    margin-top: 0.5rem;
    
    :hover {
        color: #FFFFFF;
    }
`

const ArticleItem = ({ isLiked, id, children }) => {
    const dispatch = useDispatch();
    return (
        <ItemContainer>
            <FontAwesomeIcon
                color="tomato"
                icon={[isLiked ? "fas" : "far", "heart"]}
                onClick={() => dispatch(ARTICLE_ACTIONS.likeArticle(id))} />
            <FontAwesomeIcon
                color="lightblue"
                icon={"trash"}
                onClick={() => dispatch(ARTICLE_ACTIONS.deleteArticle(id))} />
            <Item>
                {children}
            </Item>
        </ItemContainer>
    )
}

export default ArticleItem