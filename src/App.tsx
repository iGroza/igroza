import React from "react";
import { connect, useDispatch } from 'react-redux';
import styled from "styled-components";
import ArticlesButton from "./components/articles/ArticleButtons";
import ArticlesList from "./components/articles/ArticlesList";
import Loader from "./components/Loader";

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  position: relative;
  align-items: center;
  min-height: 100vh;
  height: 100%;
  width: 100%;
  background-color: #263238;
  padding-top: 10rem;
  `;

const App = ({ articles, isLoading }: Store.State["articles"]) => {
  return (
    <AppContainer>
      <ArticlesButton isLoading={isLoading}/>
      <ArticlesList articles={articles} />
      <Loader visibility={isLoading ? 1 : 0} />
    </AppContainer>
  )
};

const mapStateToProps = (state: Store.State) => state.articles;

export default connect(mapStateToProps)(App)